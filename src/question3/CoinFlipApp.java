package question3;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class CoinFlipApp extends Application{
	
	private CoinFlipGame game = new CoinFlipGame();
	
	public void start(Stage stage) {
		//create the root of the scene
		Group root = new Group(); 
		// create GameTab
		TabPane clientTabPane = new TabPane();
		Tab gameTab = new GameTab(this.game);
		clientTabPane.getTabs().add(gameTab);
	    root.getChildren().add(clientTabPane);
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 

	}
	
	 public static void main(String[] args) {
	        Application.launch(args);
	    }




}
