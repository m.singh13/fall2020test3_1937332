package question3;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class GameTab extends Tab{
	
	//private Fields
	private CoinFlipGame game;
	
	//Constructor
	public GameTab(CoinFlipGame game) {
		super("Coin Flip Game");
		this.game = game;
		createGameTab();
	}
	
	public void createGameTab() {
		VBox gameTab = new VBox();
		HBox messageBet = new HBox();
		
		TextField message = new TextField("Welcome to Flip A Coin!");
		message.setPrefWidth(300);
		
		TextField amountToBet = new TextField("Enter bet here:");
		amountToBet.setOnMouseClicked( e->{
			amountToBet.setText("");
		});
		
		messageBet.getChildren().addAll(message,amountToBet);
		
		HBox buttonsMoney = new HBox();
		HBox buttons = new HBox();
		Button heads = new Button("Heads");
		Button tails = new Button("Tails");
		buttons.getChildren().addAll(heads,tails);
		TextField currentMoney = new TextField("Current Amount : 100");
		heads.setOnAction(new CoinFlipChoice(message,amountToBet,"Heads",currentMoney,this.game));
		tails.setOnAction(new CoinFlipChoice(message,amountToBet,"Tails",currentMoney,this.game));
		buttonsMoney.getChildren().addAll(buttons,currentMoney);
		gameTab.setStyle("-fx-background-color:black;");
		gameTab.getChildren().addAll(messageBet,buttons,currentMoney);
		this.setContent(gameTab);
	}
	


}
