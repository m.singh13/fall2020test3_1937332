package question3;

import java.util.Random;

public class CoinFlipGame {
	
	//private fields
	
	private int money;
	private Random randGen = new Random();
	
	public CoinFlipGame() {
		this.money = 100;
	}
	
	public int getMoney(){return this.money;}
	
	public String playRound(String playerChoice,int amountBet) {
		
		if(amountBet >this.money) {
			return "Invalid betting amount!";
		}
		if(amountBet == 0 && this.money ==0) {
			return "You are broke!";	
		}
		if(amountBet == 0 && this.money !=0) {
			return "You did not enter a number";
		}
		
		int generatedChoice = randGen.nextInt(2);
		String computerChoice = (generatedChoice ==0 ? "Heads" : "Tails");
		String gameResult ="";
		
		if(playerChoice.equals(computerChoice)) {
			
			money += amountBet;
			gameResult ="You won!";
			
		}
		
		else {
			money -= amountBet;
			gameResult = "You lost!";
			
		}
		
		return gameResult;
	}
	

}
