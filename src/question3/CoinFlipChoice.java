package question3;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class CoinFlipChoice implements EventHandler<ActionEvent> {
	
	//private fields
	
	private TextField message;
	private TextField amountBet;
	private String playerChoice;
	private TextField currentAmount;
	private CoinFlipGame game;
	
	public CoinFlipChoice(TextField message, TextField amountBet, String playerChoice, TextField currentAmount, CoinFlipGame game) {
		this.message = message;
		this.amountBet = amountBet;
		this.playerChoice = playerChoice;
		this.currentAmount = currentAmount; 
		this.game = game;
	}
	
	public void handle(ActionEvent e) {
		int bet ;
		if(isInt(amountBet.getText())== false) { bet= 0;}
		else {bet = Integer.parseInt(amountBet.getText());}
		String gameResult = game.playRound(this.playerChoice, bet);
		this.message.setText(gameResult);
		this.currentAmount.setText("Current Amount :"+game.getMoney());
	}
	
	public static boolean isInt (String str) {
	
		try {
			int i = Integer.parseInt(str);
			return true;}
		catch(NumberFormatException er) {
			return false;
		}
	
	}

}
