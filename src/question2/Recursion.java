package question2;

public class Recursion {

	public static void main(String[] args) {
		String [] words = {"AWC","BET","ABC","WWE","WWF","WMD"};
		System.out.println("Count is "+ recursiveCount(words,0));

	}
	
	public static int recursiveCount(String[] words, int n) {
		
		    int count =0;
		    
		    if(words.length == 0) {
		    	return 0;
		    }
		    
		    if(n%2 !=1) {
		    	n++;
		    }
		    
		    if(words[n].contains(String.valueOf('W'))) {	
		    	 count++;			
			}
		    
		    if(n+2 < words.length) {
		    	return count + recursiveCount(words,n+2);
		    } 
		    
		    return count;
	    }
		
	}


